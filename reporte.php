<?php
require_once('lib/pdf/mpdf.php');
require_once 'db/conexion.php';
session_start();
$usuario = $_SESSION['usuario'];
$date = date('d/m/Y');

$boleta = $_REQUEST['Nxftgc'];

$sql = "SELECT  V_PRIMER_NOMBRE||' '||V_SEGUNDO_NOMBRE||' '||V_PRIMER_APELLIDO||' '||V_SEGUNDO_APELLIDO||' '||APELLIDO_CASADA NOMBRE, V_CUI, N_BOLETA,
                V_USUARIO_GRABA, TO_CHAR(D_FECHA_GRABA,'DD/MM/YYYY')FECHA, N_BOLETA
          FROM  TB_PARTICIPANTE_CARRERA
         WHERE  N_BOLETA = '".$boleta."'
           AND  ID_CORREL_KIT = 2
           AND ID_CARRERA_    = 2";

$ftc = oci_parse($conn, $sql);
oci_execute($ftc);

$result = oci_fetch_object($ftc);

$nombre             = $result->NOMBRE;
$cui                = $result->V_CUI;
$boleta             = $result->N_BOLETA;
$usuario_grabo      = $result->V_USUARIO_GRABA;
$fecha_grabacion    = $result->FECHA; 
$N_Boleta           =  $result->N_BOLETA; 

$sede = "SELECT N_ALCALDIA_AUX 
           FROM TB_USUARIOS_CARRERAS
          WHERE ID_USUARIO = '".$usuario_grabo."'";

    $row = oci_parse($conn, $sede);
    oci_execute($row);
    $dds = oci_fetch_object($row);

    $desc_sede = $dds->N_ALCALDIA_AUX;

    if($desc_sede != 50){
        $descripcion = "ALCALDIA AUXILIAR ZONA"." ".$desc_sede;
    }
    
  

    if($desc_sede == 50){
        $descripcion = "SEDE MUNICIPAL - EMPLEADO MUNICIPAL";
    }


    if($desc_sede == 99){
        $descripcion = "JUVENTUD Y DEPORTE";
    }

    if($desc_sede == 70){
        $descripcion = "MUNICIPALIDAD DE GUATEMALA";
    }

    if($desc_sede == 30){
        $descripcion = "DIRECCION DE LA MUJER";
    }

    if($desc_sede == 40){
        $descripcion = "DIRECCION DE LA MUJER";
    }                                
    
    
    if($desc_sede != 50 AND $desc_sede != 60 AND $desc_sede != 70 AND $desc_sede != 99 AND $desc_sede != 30 AND $desc_sede != 40){
        echo "ALCALDIA AUXILIAR ZONA"." ".$desc_sede;
    }





$html = "<header class='wrapper-img'>
        <div id='company' class='wrapper-datos'>
            <div>Inscrito por:  $usuario</div>
            <div>Fecha Impresi&oacute;n: $date</div>
        </div>
        <div id='logo' class=''>
            <img src='assets/image/logo/LOGO_5K.png' style='width: 95px;'>
        </div>


        
        <h3 class='center wrapper-muni'>Municipalidad de Guatemala</h3>
        </header>

    <div class='wrapper-container-print'>

        <div class='datos'>
            <div>PARTICIPANTE : <span class='bold'> $nombre</span> </div>
        </div>

        <div class='datos'>
            <div>CUI - DPI : <span class='bold'> $cui</span> </div>
        </div>

        <div class='datos'>
            <div># DE BOLETA : <span class='bold'> $boleta</span> </div>
        </div>

        <div class='datos'>
            <div>SEDE INSCRIPCI&Oacute;N : <span class='bold'> $descripcion</span> </div>
        </div>

        <div class='datos'>
            <div>FECHA INSCRIPCI&Oacute;N : <span class='bold'> $fecha_grabacion</span> </div>
        </div>
        

    </div>
    
    <div>
        <p class='wrapper-txt'>Esta carrera es exclusivamente para mujeres, la edad m&iacute;nima para participar debe ser 15 a&ntilde;os cumplidos antes del d&iacute;a del evento. La realizaci&oacute;n de la inscripci&oacute;n implica haber le&iacute;do, entendido y adquirido el compromiso que a continuaci&oacute;n se expone en la declaraci&oacute;n de exoneraci&oacute;n de responsabilidad y autorizaci&oacute;n:</p>
        <p>
        'Declaro que en forma voluntaria he decidido participar en la carrera 5K y que estoy en perfectas condiciones f&iacute;sicas y de salud, por lo que soy responsable de mi propia integridad f&iacute;sica. Por lo que libero a la Municipalidad de Guatemala de todo reclamo o responsabilidad de cualquier tipo que surja de su participaci&oacute;n durante o 
        posterior al evento, aunque esta responsabilidad pueda surgir por negligencia o culpa de parte de las personas nombradas en esta declaraci&oacute;n, as&iacute; como de cualquier extrav&iacute;o, robo y/o hurto que pudiera sufrir. Autorizo a los organizadores y patrocinadores al uso de fotograf&iacute;as, pel&iacute;culas, v&iacute;deos, grabaciones y 
        cualquier otro medio de registro de este evento para cualquier uso leg&iacute;timo, sin compensaci&oacute;n econ&oacute;mica alguna'.
        </p>
        <br>
        <p>Firma:______________________________</p>    
        <br>
        <p class='wrapper-space'>DPI:_______________________________</p>

        <p class='rapper-txt'>Cada participante deber&aacute; presentar un documento oficial de identificaci&oacute;n con fotograf&iacute;a. Con la COPIA debe recoger su paquete de competencia, el cual incluye n&uacute;mero, bolsa y playera.

        La entrega del paquete de competencia ser&aacute; el d&iacute;a del evento, domingo 18 de noviembre, de 5:00 a 7:00 horas. La talla de playera estar&aacute; sujeta a disponibilidad al momento de la entrega.
        </p>
    </div>

        ";

$mpdf = new mPDF('c', 'A5');
$css = file_get_contents('lib/reportes/css/style.css');
$mpdf->writeHTML($css,1);
$mpdf->WriteHTML(utf8_encode($html));
//$mpdf->writeHTML($html);
$mpdf->Output('Constancia.pdf','I');
?>
