<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>

<script>
function Error1()
    {
      swal({title:"Error al Insertar!", type:"error", showConfirmButton:false, text:"OCURRIO ALGUN ERROR!...", timer:'2000'}, 

      function () 
    {
      location.href = "menu.php?id=36"; 
    });
    }

function Insert()
    {
      swal({title:"Acceso Asignado Correctamente...!", type:"success", showConfirmButton:false, text:"DATOS GRABADOS", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=36"; 
    });
    }

function Verifica()
    {
      swal({title:"El Usuario ya cuenta con Acceso al Caso...!", type:"error", showConfirmButton:false, text:"OCURRIO ALGUN ERROR!...", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=36"; 
    });
    } 

function Seleccionar()
    {
      swal({title:"Debe Seleccionar un Caso....", type:"error", showConfirmButton:false, text:"OCURRIO ALGUN ERROR!...", timer:'2000'}, 
      function () 
    {
      location.href = "menu.php?id=36"; 
    });
    }         

</script>	
<?php
require_once 'db/conexion.php';

$cliente    = $_POST['usuario'];
$seleccion  = count($_POST['seleccion']);

if($seleccion >= 1){

    foreach($_POST['seleccion'] as $id_caso){

        $sql = mysqli_query($conn, "SELECT MAX(ID_ACCESO)+1 CONTEO
							FROM tb_acceso");

        while($row = $sql->fetch_array(MYSQLI_ASSOC)){
            $id_acceso = $row['CONTEO'];

            if($id_acceso == 0){
                $id_acceso = 1;
            }else{
                $id_acceso = $row['CONTEO'];
            }
        }

        $cuenta = mysqli_query($conn, "SELECT COUNT(*)CUENTA
								FROM tb_acceso
								WHERE ID_USUARIO = '".$cliente."'
								  AND ID_CASO = '".$id_caso."'");

        while($linea = $cuenta->fetch_array(MYSQLI_ASSOC)){
            $conteo = $linea['CUENTA'];
        }

        if($conteo == 0){

            $insert = mysqli_query($conn, "INSERT INTO tb_acceso (
                                            ID_ACCESO,
                                            ID_USUARIO,
                                            ID_CASO,
                                            FECHA_CREA)VALUES(
                                            '".$id_acceso."',
                                            '".$cliente."',
                                            '".$id_caso."',
                                            CURRENT_TIMESTAMP)
                                            ");
            
            if($insert == TRUE){
                echo "<script>Insert();</script>";
            }else{
                echo "<script>Error1();</script>";
            }
            
        }else{
            
        }/*elseif ($conteo == 1) {
                echo "<script>Verifica();</script>";
            }*/

    }

}else{
    echo "<script>Seleccionar();</script>";
}


?>