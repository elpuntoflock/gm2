(function($){
    $(document).ready(
        function(){
            // Comprobar si estamos, al menos, 100 px por debajo de la posición top
            // para mostrar o esconder el botón
            $(window).scroll(function(){
                if ( $(this).scrollTop() > 175 ) {
                    $('.scroll-to-top').fadeIn();
                    $('[data-header="all-header"]').addClass('fixed-top');
                    $('#shieldMuni').removeClass('shield-municipal-no-scroll').addClass('shield-municipal-scroll');
                } else {
                    $('.scroll-to-top').fadeOut();
                    $('[data-header="all-header"]').removeClass('fixed-top');
                    $('#shieldMuni').removeClass('shield-municipal-scroll').addClass('shield-municipal-no-scroll');
                }
            });

            // al hacer click, animar el scroll hacia arriba
            $('.scroll-to-top').click( function( e ) {
                e.preventDefault();
                $('html, body').animate( {scrollTop : 0}, 800 );
            });
        });
})(jQuery);

var items = $(".list-wrapper .list-item");
var numItems = items.length;
var perPage = 10;

items.slice(perPage).hide();

$('#pagination-container').pagination({
    items: numItems,
    itemsOnPage: perPage,
    prevText: "&laquo;",
    nextText: "&raquo;",
    onPageClick: function (pageNumber) {
        var showFrom = perPage * (pageNumber - 1);
        var showTo = showFrom + perPage;
        items.hide().slice(showFrom, showTo).show();
    }
});