$(function(){

	$.post( 'pais.php' ).done( function(respuesta)
	{
		$( '#pais' ).html( respuesta );
	});

	$('#pais').change(function()
	{
		var el_pais = $( '#pais' ).children('option:selected').val();
		
		$.post( 'departamento.php', { pais: el_pais} ).done( function( respuesta )
		{
			$( '#departamento' ).html( respuesta );
		});

	});

	$('#departamento').change(function()
	{
		var el_pais = $( '#pais' ).children('option:selected').val();
		var el_depto = $( '#departamento' ).children('option:selected').val();
		
		$.post( 'municipio.php', { pais: el_pais, depto: el_depto} ).done( function( respuesta )
		{
			$( '#municipio' ).html( respuesta );
		});

	});

})