<?php
require_once 'controller/seguridad.php';
require_once 'db/conexion.php';
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>

<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'900'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<style>
.panel-default {
	border-color: #fff !important;
}
.panel-body {
	padding: 0 !important;
}

.panel-default>.panel-heading {
	background-color: #ccc;
}

@media screen and (max-width:767px){
	.table-responsive {
		border: none !important;
	}
	.btn {
		margin-bottom: 7px !important;
	}
	.col-lg-12 {
		padding: 0 !important;
	}
}
</style>
<?php
error_reporting(E_ALL);
ini_set('display_errors','1');

$usuario 	= $_SESSION['usuario'];
$usuario 	= strtoupper($usuario);
require("calendar/functions/functions.php");
require("PHPMailer/PHPMailerAutoload.php");

// CSRF Protection
require 'calendar/functions/CSRF_Protect.php';
$csrf = new CSRF_Protect();

// Error Reporting Active
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
									FROM tb_acceso_item
									WHERE id_usuario = '".$usuario."'
										AND ITEM = ".$_REQUEST['id']."");

while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

 $resultado = $valida['CUENTA'];
}

if($resultado == 1){

}else{
echo "<script>ErrorAcceso();</script>";
}

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <!-- Custom CSS -->
    <!--link href="calendar/css/styles.css" rel="stylesheet"-->	
	<!-- DateTimePicker CSS -->
	<link href="calendar/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">	
	<!-- DataTables CSS -->
    <!--link href="calendar/css/dataTables.bootstrap.css" rel="stylesheet"-->	
	<!-- FullCalendar CSS -->
	<link href="calendar/css/fullcalendar.css" rel="stylesheet" />
	<link href="calendar/css/fullcalendar.print.css" rel="stylesheet" media="print" />	
	<!-- jQuery -->
    <script src="calendar/js/jquery.js"></script>	
	<!-- SweetAlert CSS -->
	<script src="calendar/js/sweetalert.min.js"></script> 
	<link rel="stylesheet" type="text/css" href="calendar/css/sweetalert.css">
    <!-- Custom Fonts -->
    <link href="calendar/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
	<!-- ColorPicker CSS -->
	<link href="calendar/css/bootstrap-colorpicker.css" rel="stylesheet">

</head>

	<body>
		<!-- Header -->
	   <div id="home"></div>
		<div id="eventcalendar"></div>
		<div class="content-section-a">
			
			<!--BEGIN PLUGIN -->
			<div class="container">

			<div class="">
				<div class="row">
					<div class="col-md-12">
						<div class="wrapper-logo-secondary">
							<img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
						</div>
					</div>
				</div>
			</div>

			<div class="wrapper-return">
			<a href="menu.php?id=1"><button type="button" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
			</div>			
			
			<div class="wrapper-title">
				<div class="container">
					<div class="row">
						<div class="col-md-4" data-line="mobil">
							<div class="line"></div>
						</div>
						<div class="col-md-4 section-title">
							<h1>CALENDARIO</h1>
						</div>
						<div class="col-md-4">
							<div class="line"></div>
						</div>
					</div>
				</div>
			</div>			



				<div class="row bajar">
				   <div class="col-lg-12">
				<div class="panel panel-default dash">
					<!--div class="panel panel-default"-->
						<div class="card-body">
							<!-- Button trigger New Event modal -->
							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#event">
							 <i class="fa fa-calendar" aria-hidden="true"></i> Nueva Tarea</button>
							<!-- New Event Creation Modal -->
							<div class="modal fade" id="event" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<!--button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button-->
											<h4 class="modal-title" id="myModalLabel"> <i class="fa fa-calendar" aria-hidden="true"></i> INGRESO DE TAREAS</h4>
										</div>
										<div class="modal-body">
											 <!-- Modal en la cual se ingresa las Tareas -->
											<form action="<?php echo $_SERVER['PHP_SELF'].'?id=6' ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="novoevento" onSubmit="return validacion();">
                                            <fieldset>
													<!-- CSRF PROTECTION -->
													<?php $csrf->echoInputField(); ?>
													<!-- Text input-->
													<div>
														<label>SELECCIONAR TIPO</label>
															<select name='title' class="form-control input-md">
																
																<?php 
																
																$query = mysqli_query($conection, "select * from type ORDER BY id DESC");
																
																	echo "<option value='No type Selected' required>SELECCIONAR TIPO</option>";
																	
																while ($row = mysqli_fetch_assoc($query)) {
																	  
																	echo "
																	
																	<option value='".$row['title']."'>".$row['title']."</option>
																	
																	";
																						
																  }
															
																?>
															</select>
													</div>

                                                    <div>
                                                        <label>SELECCIONAR CAUSA</label>
                                                        <select name="id_caso" id="id_caso" class="form-control abono">
                                                            <option value="-1">SELECCIONAR CASO</option>
                                                                <?php

                                                                $sql = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION,CASE 
																											WHEN A.CAUSA = '' THEN 'SIN CAUSA'
																											ELSE A.CAUSA
																											END AS DETALLE_CAUSA
                                                                                            FROM tb_caso A,
                                                                                                    tb_acceso B
                                                                                        WHERE A.ID_CASO = B.ID_CASO
                                                                                            AND B.ID_USUARIO = '".$usuario."'");


                                                                while ($row = mysqli_fetch_array($sql))
                                                                {
                                                                echo '<option value="' . $row['ID_CASO']. '">' . $row['DETALLE_CAUSA'] . '-' . $row['DESCRIPCION'] . '</option>' . "\n";
                                                                }
                                                                ?>                  
                                                        </select>
                                                    </div>
													
													<!-- Text input-->
													<div>
														<label>COLOR</label>
															<div id="cp1" class="input-group colorpicker-component">
																<input id="cp1" type="text" class="form-control" name="color" value="#5367ce" required/>
																<span class="input-group-addon"><i></i></span>
    														</div>
													</div>

													<div>
														<label>FECHA INICIO</label>
														<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd hh:ii" data-link-field="start" data-link-format="yyyy-mm-dd hh:ii">
															<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input id="ini" class="form-control" size="16" type="text" value="" readonly="" onchange="return FuncionConsulta();">
														</div>
														<input id="start" name="start" type="hidden" value="" required>
													</div>

													<div id="result" class="wrapper-space"></div>

													<div>
														<label>FECHA FINAL</label>
														<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd hh:ii" data-link-field="end" data-link-format="yyyy-mm-dd hh:ii">
															<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span><input class="form-control" size="16" type="text" value="" readonly>
														</div>
														<input id="end" name="end" type="hidden" value="">

													</div>
													
													<!-- Image input-->
													<div class="form-group" style="display: none;">
														<label class="col-md-3 control-label" for="image">Upload Image</label>
														<div class="col-md-9">
															<input type="file" name="image" id="image">
														</div>
													</div>
													
													<!-- Text input-->
													<div class="form-group" style="display: none;">
														<label class="col-md-3 control-label" for="url">Link</label>
														<div class="col-md-9">
															<input id="url" name="url" type="text" class="form-control input-md">

														</div>
													</div>

													<!-- Text input-->
													<div>
														<label>DESCRIPCI&Oacute;N</label>
                                                        <input type="text" class="form-control upper" name="description" id="description">
													</div>

													<div>
														<label>OBSERVACIONES</label>
                                                        <input type="text" class="form-control upper" name="observa" id="observa">
													</div>

													<!--div>
														<label for="">NOTIFICACI&Oacute;N</label>
														<input type="text" name="notificacion" class="form-control centrar upper" placeholder="NOTIFICACI&Oacute;N" require="">
													</div-->	

													<div style="margin-top: 15px;">
															<label for="">NOTIFICAR EMAIL No. 1</label>
															<select name="email" id="" class="form-control centrar">
																<option value="">NOTIFICAR A EMAIL No. 1</option>
																<?php

																	//require_once('db/conexion.php');

																	$email1 = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL
																																FROM tb_contacto
																																WHERE EMAIL IS NOT NULL");
																	while ($rowX = mysqli_fetch_array($email1))
																	{
																		echo '<option value="' . $rowX['EMAIL']. '">' . $rowX['EMAIL'] . '</option>' . "\n";
																	}
																	?> 							
															</select>

													<div style="text-align: left;" class="bajar">
														<!--a data-toggle='collapse' href='#demo' data-toggle="collapse"><img src="img/mas.png" style="width: 5%;"> Agregar Nuevo Correo</a-->
														<a data-toggle='collapse' href='#demo' data-toggle="collapse"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar Nuevo Correo</a>
													</div>

													</div>


													<div id="demo" class="collapse bajar">
														<label for="">NOTIFICAR EMAIL No. 2</label>
														<select name="email2" id="" class="form-control">
																<option value="">NOTIFICAR A EMAIL No. 2</option>
																<?php

																require_once('db/conexion.php');

																$email2 = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL
																															FROM tb_contacto
																															WHERE EMAIL IS NOT NULL");
																while ($rowX1 = mysqli_fetch_array($email2))
																{
																	echo '<option value="' . $rowX1['EMAIL']. '">' . $rowX1['EMAIL'] . '</option>' . "\n";
																}
																?> 								
														</select>

														<div style="text-align: left;" class="bajar">
															<a data-toggle='collapse' href='#email' data-toggle="collapse"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar Nuevo Correo</a>
														</div>

													</div>

													<div id="email" class="collapse bajar">
															<label for="">NOTIFICAR EMAIL No. 3</label>
															<select name="email3" class="form-control">
																	<option value="">NOTIFICAR A EMAIL No. 3</option>
																	<?php

																	require_once('db/conexion.php');

																	$email3 = mysqli_query($conn, "SELECT ID_CONTACTO, EMAIL
																																FROM tb_contacto
																																WHERE EMAIL IS NOT NULL");
																	while ($rowX2 = mysqli_fetch_array($email3))
																	{
																		echo '<option value="' . $rowX2['EMAIL']. '">' . $rowX2['EMAIL'] . '</option>' . "\n";
																	}
																	?> 									
															</select>
													</div>

													<!--div>
													<label for="">FECHA AVISO</label>
														<input type="text" name="fec_aviso" id="fec_notifica" class="form-control centrar upper" placeholder="Ingresar Fecha de Notificaci&oacute;n" require="">
													</div-->	

													<div>
															<label for="">DIAS PREVIOS AL AVISO:</label>
																<select name="dias" class="form-control">
																		<option value="1">1</option>
																		<option value="2">2</option>
																		<option value="3">3</option>
																		<option value="4">4</option>
																		<option value="5">5</option>
																		<option value="6">6</option>
																		<option value="7">7</option>
																		<option value="8">8</option>
																		<option value="9">9</option>
																		<option value="10">10</option>
																</select>
													</div>

													<div>
														<label>RESPONSABLE</label>
                                                        <input type="text" class="form-control upper" name="responsable" placeholder="Responsable">
													</div>																										


													<!-- Button -->
													<div class="wrapper-space">
														<input type="submit" name="novoevento" class="btn btn-success" value="GRABAR" />
														<!--button type="submit" name="novoevento" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i> GRABAR</button-->
													</div>

												</fieldset>                                              
											</form>  
										</div>
										<div class="modal-footer center">
											<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> CERRAR</button>
										</div>
									</div>
								</div>
							</div>
							<!-- Button trigger New Type modal -->
							<button type="button" class="btn btn-success" data-toggle="modal" data-target="#type">
								<i class="fa fa-globe" aria-hidden="true"></i> Nuevo Tipo
							</button>
							 <!-- New Type Creation Form -->
							<div class="modal fade" id="type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<!--button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button-->
											<h4 class="modal-title" id="myModalLabel"> <i class="fa fa-calendar" aria-hidden="true"></i>
												INGRESAR NUEVO TIPO
											</h4>
										</div>
										<div class="modal-body">                               
											<!-- New Event Creation Form -->
											<form action="<?php echo $_SERVER['PHP_SELF'].'?id=6' ?>" method="post" enctype="multipart/form-data" class="form-horizontal" name="novotipo">
												<fieldset>
													<!-- CSRF PROTECTION -->
													<?php $csrf->echoInputField(); ?>
													<!-- Text input-->
													<div>
														<label>INGRESAR TIPO</label>
													    <input id="title" name="title" type="text" class="form-control upper" required>
													</div>

													<!-- Button -->
													<div class="wrapper-space">
													 <input type="submit" name="novotipo" class="btn btn-success" value="Grabar" />
													</div>

												</fieldset>
											</form>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Button trigger Delete Event modal -->
							<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editevent">
								<i class="fa fa-edit" aria-hidden="true"></i> Editar Tarea
							</button>

							<!-- Modal -->
							<div class="modal fade" id="editevent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog modal-lg" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<!--button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button-->
											<h4 class="modal-title" id="myModalLabel"> <i class="fa fa-calendar" aria-hidden="true"></i>
											EDITAR TAREAS
											</h4>
										</div>
										<div class="modal-body">
											<!-- Modal featuring all events saved on database -->
											<?php echo listAllEventsEdit(); ?> 

										</div>
										<div class="modal-footer bajar">
											<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
							
							<!-- Button trigger Delete Event modal -->
							<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#delevent">
								<i class="fa fa-trash" aria-hidden="true"></i> Eliminar Tareas
							</button>

							<!-- Modal -->
							<div class="modal fade" id="delevent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<!--button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button-->
											<h4 class="modal-title" id="myModalLabel"> <i class="fa fa-calendar" aria-hidden="true"></i>
												ELIMINAR TAREAS
											</h4>
										</div>
										<div class="modal-body">
											<!-- Modal featuring all events saved on database -->
											<?php 
												echo listAllEventsDelete();
											?>

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>

							<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deltype">
								<i class="fa fa-times-circle" aria-hidden="true"></i> Eliminar Tipos
							</button>

							<!-- Modal -->
							<div class="modal fade" id="deltype" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<!--button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button-->
											<h4 class="modal-title" id="myModalLabel3"> <i class="fa fa-calendar" aria-hidden="true"></i>
												ELIMINAR TIPOS
											</h4>
										</div>
										<div class="modal-body">
											<!-- Modal featuring all types saved on database -->
											<?php echo listAllTypes(); ?>

										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /.panel-heading -->
						<div class="panel-body">
							<div class="table-responsive">

								<div class="col-lg-12">
									<div id="events"></div>
								</div>
							</div>
						</div>
					<!--/div-->
				</div>
			</div>
			<?php

				// If user clicked on the new event button
				if (!empty($_POST['novoevento'])) {
					
					// Variables from form
                            $title = $_POST['title'];
                            $id_caso = $_POST['id_caso'];
							$image = $_FILES['image'];
                            $description = strtoupper(trim(preg_replace('/\s+/', ' ',nl2br($_POST['description']))));
                            $observacion = strtoupper(trim(preg_replace('/\s+/', ' ',nl2br($_POST['observa']))));			
							$url = $_POST['url'];
							$start = $_POST['start'];
							$end = $_POST['end'];
							$color = $_POST['color'];
							$responsable = strtoupper($_POST['responsable']);

					//Variables para Notificaciones		
							$notificacion 	= " ";
							//$notificacion 	= strtoupper($_POST['notificacion']);
							//$fecha_aviso1	= $_POST['fec_aviso'];
							$dias 			= $_POST['dias'];

							
					if (empty($start) || empty($end)) {
						echo "<script type='text/javascript'>swal('Ooops...!', 'You need to fill in the date options!', 'error');</script>";	
						echo '<meta http-equiv="refresh" content="1; menu.php?id=6">'; 
						return false;
					}
							
					if (!empty($start) || !empty($end) || !empty($image)) {
				 
						// If photos has been slected
						if (!empty($image["name"]) && isset($_FILES['image'])) {
					 
							// Max width (px)
							$largura = 10000;
							// Max high (px)
							$altura = 10000;
							// Max size (pixels)
							$tamanho = 5000000000;
					 
							// Verifies if this is an image format
							if(!preg_match("/image\/(pjpeg|jpeg|png|gif|bmp)/", $image["type"])){
							   $error[1] = "Sorry but this is not an image.";
							} 
					 
							// Select image size
							$dimensoes = getimagesize($image["tmp_name"]);
					 
							// check if the width size is allowed
							if($dimensoes[0] > $largura) {
								$error[2] = "Image width should be max ".$largura." pixels";
							}
					 
							// check if the height size is allowed
							if($dimensoes[1] > $altura) {
								$error[3] = "Image height should be max ".$altura." pixels";
							}
					 
							// check if the total size is allowed
							if($image["size"] > $tamanho) {
								$error[4] = "Image Should have max ".$tamanho." bytes";
							}
							
								// Get image extension
								preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $image["name"], $ext);
					 
								// Creates unique name (md5)
								$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
					 
								// Path for uploading the image
								$caminho_imagem = "calendar/uploads/" . $nome_imagem;
					 
								// upload the image to the folder
								move_uploaded_file($image["tmp_name"], $caminho_imagem);

								// Saves informationon the database
								$val_event = mysqli_query($conn, "SELECT MAX(id)+1 CONTEO_N
																	FROM events");

									while($row = $val_event->fetch_array(MYSQLI_ASSOC)){
										$id = $row['CONTEO_N'];
							
										if($id == 0){
											$id = 1;
										}else{
											$id = $row['CONTEO_N'];
										}
									}	

									$sql = mysqli_query($conection, "INSERT INTO events VALUES ('".$id."', '".$id_caso."','".$title."', '','".str_replace( "'", "´", $description)."','".str_replace( "'", "´", $observacion)."','".$start."','".$end."','".$url."', '".$color."','".$usuario."',CURRENT_TIMESTAMP, '".$responsable."')");

								// If information is correctly saved		
								if (!$sql) {
								echo ("Can't insert into database: " . mysqli_error());
								return false;
								} else {
										echo "<script type='text/javascript'>swal('Datos Grabados con Exito...!', 'TAREA GRABADA...!', 'success');</script>";
										echo '<meta http-equiv="refresh" content="1; menu.php?id=6">'; 
										die();
								}		
								return true;

							// Displays any error on database saving
							if (count($error) != 0) {
								foreach ($error as $erro) {
									echo $erro . "<br />";
								}
							}
						}
					} if (empty($image["name"])) {

						// Saves informationon the database
						$val_events = mysqli_query($conn, "SELECT MAX(id)+1 CONTEO_N
															FROM events");

						while($rowX = $val_events->fetch_array(MYSQLI_ASSOC)){
							$id = $rowX['CONTEO_N'];

						if($id == 0){
							$id = 1;
						}else{
							$id = $rowX['CONTEO_N'];
						}
						}
							$sql = mysqli_query($conection, "INSERT INTO events VALUES ('".$id."', '".$id_caso."','".$title."', '','".str_replace( "'", "´", $description)."','".str_replace( "'", "´", $observacion)."','".$start."','".$end."','".$url."', '".$color."','".$usuario."',CURRENT_TIMESTAMP, '".$responsable."')");

							// If information is correctly saved		
							if (!$sql) {
							echo ("Can't insert into database: " . mysqli_error());
							return false;
							} else {

/******************************************************************************************/
					// Variables from form
					$title = $_POST['title'];
					$id_caso = $_POST['id_caso'];
					$image = $_FILES['image'];
					$description = strtoupper(trim(preg_replace('/\s+/', ' ',nl2br($_POST['description']))));
					$observacion = strtoupper(trim(preg_replace('/\s+/', ' ',nl2br($_POST['observa']))));
					$url = $_POST['url'];
					$start = $_POST['start'];
					$end = $_POST['end'];
					$color = $_POST['color'];
					$responsable = strtoupper($_POST['responsable']);

					//Variables para Notificaciones		
					//$notificacion 	= strtoupper($_POST['notificacion']);
					$notificacion 	= " ";
					//$fecha_aviso1	= $_POST['fec_aviso'];
					$dias 			= $_POST['dias'];

					$fec_avi = mysqli_query($conn, "SELECT '$start' - interval '$dias' day as FECHA
					FROM DUAL");

					while($txt = $fec_avi->fetch_array(MYSQLI_ASSOC)){
						$fecha_aviso = $txt['FECHA'];
					}

	
	if(isset($_POST['email']))
	{
		$email = $_POST['email']; 
	}
	else{
		$email = null;        
	}	
	

	if(isset($_POST['email2']))
	{
		$email2 = $_POST['email2']; 
	}
	else{
		$email2 = null;        
	}	


	if(isset($_POST['email3']))
	{
		$email3 = $_POST['email3']; 
	}
	else{
		$email3 = null;        
	}		

	if($email == null){
		//echo "No trae Email no realiza Insert";
	}else{
		//$newDate 		= date("Y/m/d", strtotime($fecha_aviso1));

		$val_noti = mysqli_query($conection, "SELECT MAX(ID_NOTIFICA)+1 CONTEO
										   FROM tb_notifica");

		while($rest = $val_noti->fetch_array(MYSQLI_ASSOC)){
			$id_notifica = $rest['CONTEO'];

			if($id_notifica == 0){
				$id_notifica = 1;
			}else{
				$id_notifica = $rest['CONTEO'];
			}
		}

		$val = mysqli_query($conection,"select CAUSA, DESCRIPCION
							from tb_caso
							where ID_CASO = '".$id_caso."'");

			while($val_res = $val->fetch_array(MYSQLI_ASSOC)){
				$deta_causa = $val_res['CAUSA'];
				$descrip_caso = $val_res['DESCRIPCION'];
			}				


		$val_eventX = mysqli_query($conection, "SELECT MAX(id)+1 CONTEO_N
												FROM events");

		while($rowS = $val_eventX->fetch_array(MYSQLI_ASSOC)){
			$id_1 = $rowS['CONTEO_N'];

			if($id_1 == 0){
				$id_1 = 1;
			}else{
				$id_1 = $rowS['CONTEO_N'];
			}
		}
		
		$Num_cel = mysqli_query($conn, "SELECT TELEFONO
		FROM tb_contacto
		WHERE email = '".$email."'");
	   
		while($ftg = $Num_cel->fetch_array(MYSQLI_ASSOC)){
			$movil = $ftg['TELEFONO'];
		}		

		$ingreso = "INSERT INTO tb_notifica (
							id_notifica, 
							id, 
							notificacion, 
							email, 
							fecha_notifica, 
							fecha_tarea, 
							fecha_crea, 
							dias_notifica,
							fecha_enviado,
							celular)
							VALUES(
							'".$id_notifica."', 
							'".$id."', 
							'".$notificacion."', 
							'".$email."', 
							'".$fecha_aviso."', 
							'".$start."', 
							CURRENT_TIMESTAMP, 
							'".$dias."',
							'',
							'".$movil."')
							";
		mysqli_query($conection, $ingreso);					

if($ingreso == TRUE){
	$mail = new PHPMailer;

	$mensaje = "CAUSA No. $deta_causa <br> DESCRIPCI&Oacute;N DE CAUSA: $descrip_caso <br><br> TIPO DE TAREA: $title. <br><br> Por este medio se le informa que ha sido invitado a participar en la actividad $description relacionada al caso arriba mencionado.
				 <br> Cualquier duda o informaci&oacute;n estaremos en contacto con su persona. <br><br><br>  Gracias.";
	
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'mx1.hostinger.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'aboga@e-softwareweb.net';                 // SMTP username
	$mail->Password = '@bog@nster';                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to
	
	$mail->setFrom('aboga@e-softwareweb.net', 'Aboga');
	$mail->addAddress($email);  
	$mail->isHTML(true);   
	
	$mail->Subject = 'RECORDATORIO CAUSA No.'.$deta_causa;
	$mail->Body    = $mensaje;
	
	if(!$mail->send()) {
		//echo 'Error, mensaje no enviado';
		//echo 'Error del mensaje: ' . $mail->ErrorInfo;
	} else {
		//echo "<script>Enviado();</script>";
		
	}
}else{
	echo "Ocurrio Algun Error";
}									


	}	




	if($email2 == null){
		//echo "No trae Email no realiza Insert";
	}else{
		//$newDate 		= date("Y/m/d", strtotime($fecha_aviso1));

		$val_noti2 = mysqli_query($conection, "SELECT MAX(ID_NOTIFICA)+1 CONTEO
										   FROM tb_notifica");

		while($rest2 = $val_noti2->fetch_array(MYSQLI_ASSOC)){
			$id_notifica2 = $rest2['CONTEO'];

			if($id_notifica2 == 0){
				$id_notifica2 = 1;
			}else{
				$id_notifica2 = $rest2['CONTEO'];
			}
		}

		$val1 = mysqli_query($conection,"select CAUSA, DESCRIPCION
							from tb_caso
							where ID_CASO = '".$id_caso."'");

			while($val_res1 = $val1->fetch_array(MYSQLI_ASSOC)){
				$deta_causa1 = $val_res1['CAUSA'];
				$descrip_caso1 = $val_res1['DESCRIPCION'];
			}		

		$val_eventX2 = mysqli_query($conection, "SELECT MAX(id)+1 CONTEO_N
												FROM events");

		while($rowS2 = $val_eventX2->fetch_array(MYSQLI_ASSOC)){
			$id_2 = $rowS2['CONTEO_N'];

			if($id_2 == 0){
				$id_2 = 1;
			}else{
				$id_2 = $rowS2['CONTEO_N'];
			}
		}
		
		$Num_cel_1 = mysqli_query($conn, "SELECT TELEFONO
		FROM tb_contacto
		WHERE email = '".$email2."'");
	   
	while($ftg_1 = $Num_cel_1->fetch_array(MYSQLI_ASSOC)){
		$movil_1 = $ftg_1['TELEFONO'];
	}   		

		$ingreso2 = "INSERT INTO tb_notifica (
							id_notifica, 
							id, 
							notificacion, 
							email, 
							fecha_notifica, 
							fecha_tarea, 
							fecha_crea, 
							dias_notifica,
							fecha_enviado,
							celular)
							VALUES(
							'".$id_notifica2."', 
							'".$id."', 
							'".$notificacion."', 
							'".$email2."', 
							'".$fecha_aviso."', 
							'".$start."', 
							CURRENT_TIMESTAMP, 
							'".$dias."',
							'',
							'".$movil_1."')
							";
		mysqli_query($conection, $ingreso2);					

if($ingreso2 == TRUE){
	$mail = new PHPMailer;

	$mensaje = "CAUSA No. $deta_causa1 <br><br> DESCRIPCI&Oacute;N DE CAUSA: $descrip_caso1. <br><br> TIPO DE TAREA: $title. <br> Por este medio se le informa que ha sido invitado a participar en la actividad $description relacionada al caso arriba mencionado.
				 <br> Cualquier duda o informaci&oacute;n estaremos en contacto con su persona. <br><br><br>  Gracias.";
	
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'mx1.hostinger.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'aboga@e-softwareweb.net';                 // SMTP username
	$mail->Password = '@bog@nster';                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to
	
	$mail->setFrom('aboga@e-softwareweb.net', 'Aboga');
	$mail->addAddress($email2);  
	$mail->isHTML(true);   
	
	$mail->Subject = 'RECORDATORIO CAUSA No.'.$deta_causa1;
	$mail->Body    = $mensaje;
	
	if(!$mail->send()) {
		//echo 'Error, mensaje no enviado';
		//echo 'Error del mensaje: ' . $mail->ErrorInfo;
	} else {
		//echo "<script>Enviado();</script>";
		
	}
}else{
	echo "Ocurrio Algun Error";
}									


	}	
	
	
	if($email3 == null){
		//echo "No trae Email no realiza Insert";
	}else{
		//$newDate 		= date("Y/m/d", strtotime($fecha_aviso1));

		$query_val = mysqli_query($conection, "SELECT MAX(ID_NOTIFICA)+1 CONTEO
										   FROM tb_notifica");

		while($result = $query_val->fetch_array(MYSQLI_ASSOC)){
			$id_not = $result['CONTEO'];

			if($id_not == 0){
				$id_not = 1;
			}else{
				$id_not = $result['CONTEO'];
			}
		}


		$val2 = mysqli_query($conection,"select CAUSA, DESCRIPCION
							from tb_caso
							where ID_CASO = '".$id_caso."'");

			while($val_res2 = $val2->fetch_array(MYSQLI_ASSOC)){
				$deta_causa2 = $val_res2['CAUSA'];
				$descrip_caso2 = $val_res2['DESCRIPCION'];
			}			


		$valor_event = mysqli_query($conection, "SELECT MAX(id)+1 CONTEO_N
												FROM events");

		while($rep = $valor_event->fetch_array(MYSQLI_ASSOC)){
			$id_evento = $rep['CONTEO_N'];

			if($id_evento == 0){
				$id_evento = 1;
			}else{
				$id_evento = $rep['CONTEO_N'];
			}
		}
		
        $Num_cel_2 = mysqli_query($conn, "SELECT TELEFONO
                                            FROM tb_contacto
                                            WHERE email = '".$email3."'");
       
        while($ftg_2 = $Num_cel_2->fetch_array(MYSQLI_ASSOC)){

             $movil_2 = $ftg_2['TELEFONO'];

        }		

		$ingreso3 = "INSERT INTO tb_notifica (
							id_notifica, 
							id, 
							notificacion, 
							email, 
							fecha_notifica, 
							fecha_tarea, 
							fecha_crea, 
							dias_notifica,
							fecha_enviado,
							celular)
							VALUES(
							'".$id_not."', 
							'".$id."', 
							'".$notificacion."', 
							'".$email3."', 
							'".$fecha_aviso."', 
							'".$start."', 
							CURRENT_TIMESTAMP, 
							'".$dias."',
							'',
							'".$movil_2."')
							";
		mysqli_query($conection, $ingreso3);					

if($ingreso3 == TRUE){
	$mail = new PHPMailer;

	$mensaje = "CAUSA No. $deta_causa2 <br><br> DESCRIPCI&Oacute;N DE CAUSA: $descrip_caso2. <br><br> TIPO DE TAREA: $title. <br> Por este medio se le informa que ha sido invitado a participar en la actividad $description relacionada al caso arriba mencionado.
				 <br> Cualquier duda o informaci&oacute;n estaremos en contacto con su persona. <br><br><br>  Gracias.";
	
	$mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'mx1.hostinger.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'aboga@e-softwareweb.net';                 // SMTP username
	$mail->Password = '@bog@nster';                           // SMTP password
	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to
	
	$mail->setFrom('aboga@e-softwareweb.net', 'Aboga');
	$mail->addAddress($email3);  
	$mail->isHTML(true);   
	
	$mail->Subject = 'RECORDATORIO CAUSA No.'.$deta_causa2;
	$mail->Body    = $mensaje;
	
	if(!$mail->send()) {
		//echo 'Error, mensaje no enviado';
		//echo 'Error del mensaje: ' . $mail->ErrorInfo;
	} else {
		//echo "<script>Enviado();</script>";
		
	}
}else{
	echo "Ocurrio Algun Error";
}									


	}		


/*****************************************************************************************/	


									echo "<script type='text/javascript'>swal('Datos Grabados con Exito...!', 'TAREA GRABADA...!', 'success');</script>";
									echo '<meta http-equiv="refresh" content="1; menu.php?id=6">'; 
									die();
							}		
							return true;

					}

				}


				// If user clicked on the new event button
				if (!empty($_POST['novotipo'])) {
				 
					// Variables from form
					$title = strtoupper($_POST['title']);
					
					// Saves informationon the database
					$sql = mysqli_query($conection, "INSERT INTO type VALUES ('', '".$title."')");
		 
					// If information is correctly saved			
					if (!$sql) {
					echo ("Can't insert into database: " . mysqli_error());
					return false;
					} else {
							echo "<script type='text/javascript'>swal('Datos Grabados ....!', 'Nuevo tipo grabado....!', 'success');</script>";
							echo '<meta http-equiv="refresh" content="1; menu.php?id=6">'; 
							die();
					}		
					return true;
				}

			?>
			<!-- Modal with events description -->
			<?php echo modalEvents(); ?>
				</div>

			</div>
			<!-- /.container -->

		</div>
		
		<!-- Plugin Description -->
		<div id="features"></div>	

		

		<!-- Bootstrap Core JavaScript -->
		<!--script src="calendar/js/bootstrap.min.js"></script-->
		<!-- DataTables JavaScript -->
		<!--script src="calendar/js/jquery.dataTables.js"></script>
		<script src="calendar/js/dataTables.bootstrap.js"></script-->
		<!-- Listings JavaScript delete options-->
		<script src="calendar/js/listings.js"></script>
		<!-- Metis Menu Plugin JavaScript -->
		<script src="calendar/js/metisMenu.min.js"></script>
		<!-- Moment JavaScript -->
		<script src="calendar/js/moment.min.js"></script>
		<!-- FullCalendar JavaScript -->
		<script src="calendar/js/fullcalendar.js"></script>
		<!-- FullCalendar Language JavaScript Selector -->
		<script src='calendar/lang/en-gb.js'></script>
		<!-- DateTimePicker JavaScript -->
		<script type="text/javascript" src="calendar/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
		<!-- Datetime picker initialization -->
		<script type="text/javascript">
			$('.form_date').datetimepicker({
				language:  'en',
				weekStart: 1,
				todayBtn:  0,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				forceParse: 0
			});
		</script>	
		<!-- ColorPicker JavaScript -->
		<script src="calendar/js/bootstrap-colorpicker.js"></script>
		<!-- Plugin Script Initialization for DataTables -->
		<script>
			$(document).ready(function() {
				$('#dataTables-example').dataTable({
					"language": {
            "url": "assets/js/Spanish.json"
        }
				});
			});
		</script>
		<!-- ColorPicker Initialization -->
		<script>
			$(function() {
				$('#cp1').colorpicker();
			});
		
		</script>
		<!-- JS array created from database -->
		<?php echo listEvents(); ?>
		
		
		
	</body>

</html>
<script>
	function validacion() {

		var fecha_inicio = document.getElementById('start').value;
		var fecha_final  = document.getElementById('end').value;

		if(fecha_final < fecha_inicio){
			alert('Fecha Final no puede ser menor a la Fecha Inicio, Verificar las Fechas...!!!');
			return false;
		}
		
	}
</script>


<script>

function FuncionConsulta() {
    var formData = new FormData();
    formData.append('start',  document.getElementById("ini").value);
	formData.append('caso',  document.getElementById("id_caso").value);
     
	$.ajax({
	url:  "controller/date_event.php",
	type: "POST",
	dataType: "text",
	data: formData,
	cache: false,
	contentType: false,
	processData: false,

		})
	.done(function(res){
	$("#result").html( res);
	});

}

</script>