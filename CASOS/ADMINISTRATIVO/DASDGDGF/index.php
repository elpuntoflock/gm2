<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    <title>Document</title>
</head>
<body>
    <form action="insert.php" method="post">

        <div class="col-md-12 table-responsive">
            <div class="col-md-12 wrapper-space">
                <table id="dynamic_field" class="table table-striped table-bordered ">
                    <thead>
                        <tr>
                        <th class="wrapper-centrar">Dato 1</th>
                        <th class="wrapper-centrar">Dato 2</th>
                        <th class="wrapper-centrar">Dato 3</th>
                        <th class="wrapper-centrar">Dato 4</th>
                        <th class="wrapper-centrar">Dato 5</th>
                        <th class="wrapper-centrar"><i class="fa fa-plus-circle" aria-hidden="true"></i> Detalle</th>                        
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <div class="col-md-2">
                                    <input type="text" name="dato1[]" id="dato1" class="form-control upper" placeholder="Dato 1" style="width: 250px;">    
                                </div>
                            </td>
                            <td>
                                <div class="col-md-4">
                                    <input type="text" name="dato2[]" id="dato2" class="form-control upper" placeholder="Dato 2" style="width: 250px;">
                                </div>                        
                             </td>
                             <td>
                                <div class="col-md-4">
                                    <input type="text" name="dato3[]" id="dato3" class="form-control upper" placeholder="Dato 3" style="width: 250px;">
                                </div>
                             </td>
                             <td>
                                <div class="col-md-4">
                                    <input type="text" name="dato4[]" id="dato4" class="form-control center" placeholder="Dato 4" style="width: 250px;">
                                </div>                       
                             </td>
                             <td>
                                <div class="col-md-4">
                                    <input type="text" name="dato5[]" id="dato5" class="form-control center" placeholder="Dato 5" style="width: 250px;">
                                </div>
                             </td>
                             <td>
                                <div class="col-md-4">
                                    <button type="button" name="add" id="add" class="btn btn-primary boton_add">+</button>
                                </div>
                             </td>                              
                        </tr>
                    </tbody>
                </table>  
                <div class="col-md-2">
                    <button type="submit" id="boton" class="btn btn-success">GRABAR</button>
                </div>          
            </div>
        </div>
    </form>        
</body>
</html>

<script>
    $(document).ready(function(){
        var i=0;
        
        $('#add').click(function(){
            i++;
            
            $('#dynamic_field').append('<tr id="row'+i+'"><td><div class="col-md-2"><input name="dato1[]" id="dato1'+i+'" data="dato1'+i+'" placeholder="Dato 1" class="form-control" style="width: 250px;"></div></td><td><div class="col-md-4"><input type="text" name="dato2[]" id="dato2'+i+'" class="form-control" placeholder="Dato 2" style="width: 250px;"></div></td><td><div class="col-md-4"><input type="text" name="dato3[]" id="dato3'+i+'" class="form-control upper" placeholder="Dato 3" style="width: 250px;"></div></td><td><div class="col-md-4"><input type="text" name="dato4[]" id="dato4'+i+'" class="form-control center" placeholder="Dato 4" style="width: 250px;"></div></td><td><div class="col-md-4"><input type="text" name="dato5[]" id="dato5'+i+'" class="form-control center monto" placeholder="Dato 5" style="width: 250px;"></div></td><td class="wrapper-centrar"><button type="button" id="'+i+'" class="btn btn-danger btn_remove" name="remove"><i class="fa fa-minus-circle" aria-hidden="true"></i>X</button></td></tr>');
            
        })

        $(document).on('click','.btn_remove', function (){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        })

    })
</script>