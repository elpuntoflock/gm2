<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <title>Aboga</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="login/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="login/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="login/assets/css/form-elements.css">
        <link rel="stylesheet" href="login/assets/css/style.css">

        <!-- Favicon and touch icons -->
        <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="login/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="login/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="login/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="login/assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body onload="deshabilitaRetroceso();">
        <div class="background-login">

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="wrapper-title-main">
                            <h1>Iniciar <br> <span>Sesi&oacute;n</span> </h1>
                            <div class="underline"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Top content -->
            <div class="box-form-login">

            <div>
                       <div class="container">
                           <div class="row">
                               <div class="col-sm-6 col-sm-offset-3 form-box">
                                   <div class="form-top">
                                       <div class="form-top-left" style='display: inline-block;'>
                                           <img src="login/assets/img/backgrounds/balance.png">
                                           <h3 class="color">Aboga</h3>
                                           <!--p class="color">Ingrese nombre de usuario y contraseña para iniciar sesi&oacute;n:</p-->
                                       </div>
                                       <div class="form-top-right">
                                           <i class="fa fa-lock"></i>
                                       </div>
                                   </div>
                                   <div class="form-bottom">
                                       <form role="form" action="chek_user.php" method="post" class="login-form spacing">
                                           <div class="form-group">
                                               <label class="sr-only" for="form-username">Usuario</label>
                                               <input type="text" name="usuario" placeholder="Usuario" class="form-username form-control" id="form-username" autofocus="">
                                           </div>
                                           <div class="form-group">
                                               <label class="sr-only" for="form-password">Password</label>
                                               <input type="password" name="password" placeholder="Contrase&ntilde;a...!!!" class="form-password form-control" id="contrasena">
                                           </div>
                                           <div class="centrar">
                                           <span><input type="checkbox" id="mostrar_contrasena"> Mostrar Contrase&ntilde;a</span>
                                           </div>
                                           <center><button type="submit" class="boton3 spacing">INGRESAR</button></center>
                                       </form>
                                   </div>
                                   <div class="wrapper-olvida">
                                   <a href="#" data-toggle="modal" data-target="#myModal">&iquest;Olvid&oacute; su Contrase&ntilde;a?</a>
                                   </div>
                                   
                               </div>
                           </div>
                       </div>
                   </div>

            </div>
            <div class="push"></div>
        </div>
        <div class="wrapper-footer-login footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                       <div class="box-footer">
                           <p>Powered by e-software </p>
                           <p>Guatemala <?php echo date("Y") ?></p>
                       </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Javascript -->
        <script src="login/assets/js/jquery-1.11.1.min.js"></script>
        <script src="login/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="login/assets/js/jquery.backstretch.min.js"></script>
        <script src="login/assets/js/scripts.js"></script>
    </body>


</html>

 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reinicio de Password</h4>
        </div>
        <div class="modal-body">
            <form action="envia_email.php" method="post">
            <div>
                <label>Correo Electronico</label>
                <input type="text" class="form-control centrar" name="email" id="" placeholder="Ingresar Email..!!!" autofocus="" require="">
            </div>
                
            <div>
                <button type="submit" class="boton3 spacing">Enviar</button>
            </div>
            </form>
        </div>
      </div>
      
    </div>
  </div>

  <script>
    function deshabilitaRetroceso(){
    window.location.hash="no-back-button";
    window.location.hash="Again-No-back-button" //chrome
    window.onhashchange=function(){window.location.hash="no-back-button";}
}
</script>
<script>
    $(document).ready(function (){
        $('#mostrar_contrasena').click(function (){
            if($('#mostrar_contrasena').is(':checked')){
                $('#contrasena').attr('type','text');
            }else{
                $('#contrasena').attr('type','password');
            }
        });
    });
</script>