<?php
require_once '../fpdf/fpdf.php';
require_once '../db/conexion.php';

$direccion = utf8_decode('Dirección');
$telefono  = utf8_decode('Teléfono');

$detalle = mysqli_query($conn, "SELECT ID_CONTACTO, TRIM(CONCAT_WS(' ', NOMBRES,' ',APELLIDOS,' ',NOMBRE_EMPRESA))NOMBRE, CUI, TELEFONO,
                                        NIT, DIRECCION, EMAIL
                                FROM tb_contacto");

$pdf = new FPDF('L');
$pdf->AddPage();
$pdf->SetFont('Arial','',9);

$pdf->Image('../img/logo/Law.jpg',25,15,35,0);

$pdf->SetY(50);

$pdf->Cell(260,10,'REPORTE GENERAL DE CONTACTOS',0,1,'C');

$pdf->Cell(80,5,'Nombre', 1,0,'C');
$pdf->Cell(18,5,$telefono, 1,0,'C');
$pdf->Cell(18,5,'Nit', 1,0,'C');
$pdf->Cell(65,5,$direccion, 1,0,'C');
$pdf->Cell(65,5,'Correo Electronico', 1,1,'C');
  
while ($rest = mysqli_fetch_array($detalle)){

    $id_contacto = $rest[0];
    $nombre      = $rest[1];
    $cui         = $rest[2];
    $telefono    = $rest[3];
    $nit         = $rest[4];
    $direccion   = strtolower($rest[5]);
    $email       = strtolower($rest[6]);

    $pdf->Cell(80,5, $nombre , 1, 0, 'L');
    $pdf->Cell(18,5, $telefono ,1, 0, 'C');
    $pdf->Cell(18,5, $nit ,1, 0, 'C');
    $pdf->Cell(65,5, $direccion ,1, 0, 'L');
    $pdf->Cell(65,5, $email ,1, 1, 'L');

}

$pdf->Output();

?>