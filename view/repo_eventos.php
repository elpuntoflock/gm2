<?php
require_once '../fpdf/fpdf.php';
require_once '../db/conexion.php';
session_start();
$usuario 	= $_SESSION['usuario'];
$usuario 	= strtoupper($usuario);

$caso = $_REQUEST['ftc'];


$dbd = mysqli_query($conn,"SELECT CAUSA, ID_CASO
        FROM tb_caso
        WHERE ID_CASO = '".$caso."'");

while($roww = $dbd->fetch_array(MYSQLI_ASSOC)){
    $causa = $roww['CAUSA'];
}  



$tareas = mysqli_query($conn, "SELECT A.ID, A.DESCRIPTION, DATE_FORMAT(A.START,'%d/%m/%Y %H:%i:%s') FECHA, DATE_FORMAT(A.END,'%d/%m/%Y %H:%i:%s') FECHA1, 
                                        TITLE, A.OBSERVACIONES, A.responsable
                                FROM events A,
                                     tb_acceso B
                                WHERE A.ID_CASO     = B.ID_CASO
                                AND A.ID_CASO       = '".$caso."'
                                AND B.ID_USUARIO    = '".$usuario."'
                                ORDER BY FECHA ASC");

$pdf = new FPDF('L');
$pdf->AddPage();
$pdf->SetFont('Arial','',7);


$pdf->Image('../img/logo/Law.jpg',25,15,35,0);

$pdf->SetY(50);

$pdf->Cell(260,10,'Reporte de Tareas del Caso '.$caso.' - '.$causa.'' ,0,1,'C');
$pdf->Ln();
$pdf->Cell(20,4,'TIPO TAREA',1,0,'C');
$pdf->Cell(80,4,'DESCRIPCION TAREA',1,0,'C');
$pdf->Cell(50,4,'RESPONSABLE', 1,0,'C');
$pdf->Cell(30,4,'FECHA INICIO', 1,0,'C');
$pdf->Cell(75,4,'OBSERVACIONES', 1,1,'C');


while ($rest = mysqli_fetch_array($tareas)){

    $pdf->Cell(20,8, "$rest[4]",1,0);
    $pdf->Cell(80,8, "$rest[1]",1,0);
    $pdf->Cell(50,8, "$rest[6]",1,'C');
    $pdf->Cell(30,8, "$rest[2]",1,0,'C');
    $pdf->MultiCell(75,8, "$rest[5]",1,1);

}

$pdf->SetY(160);

$pdf->Cell(250,10,'Firma:____________________________________________',0,1,'C');

$pdf->Output();
?>