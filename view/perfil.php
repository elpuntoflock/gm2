<?php
require_once 'controller/controller.php';
$usuario    = strtoupper($_SESSION['usuario']);
?>
<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-users" aria-hidden="true"></i> PERFIL</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<form action="menu.php?id=34" method="post">
    <div class="col-md-12">
        <div class="col-md-4 offset-md-4 wrapper-centrar">
            <label>PRIMER NOMBRE</label>
            <input type="text" class="form-control centrar" name="p_nombre" value="<?php echo $pnombre; ?>" require="">
        </div>
    </div>

    <div class="col-md-12">
        <div class="col-md-4 offset-md-4 wrapper-centrar wrapper-space">
            <label>SEGUNDO NOMBRE</label>
            <input type="text" class="form-control centrar" name="s_nombre" value="<?php echo $snombre; ?>">        
        </div>    
    </div>

    <div class="col-md-12">
        <div class="col-md-4 offset-md-4 wrapper-centrar wrapper-space">
            <label>PRIMER APELLIDO</label>
            <input type="text" class="form-control centrar" name="p_apellido" value="<?php echo $papellido; ?>" require="">
        </div>    
    </div>

    <div class="col-md-12">
        <div class="col-md-4 offset-md-4 wrapper-centrar wrapper-space">
            <label>SEGUNDO APELLIDO</label>
            <input type="text" class="form-control centrar" name="s_apellido" value="<?php echo $sapellido; ?>">
        </div>    
    </div>

    <div class="col-md-12">
        <div class="col-md-4 offset-md-4 wrapper-centrar wrapper-space">
            <label>EMAIL</label>
            <input type="text" class="form-control centrar" name="email" value="<?php echo $email; ?>" require="">
        </div>    
    </div>

    <div class="col-md-12">
        <div class="col-md-4 offset-md-4 wrapper-centrar wrapper-space">
            <label>CELULAR</label>
            <input type="text" class="form-control centrar" name="celular" value="<?php echo $celular; ?>" require="">
        </div>    
    </div>            

    <div class="col-md-12">
        <div class="col-md-4 offset-md-4 wrapper-centrar wrapper-space">
            <button type="submit" class="btn btn-success">GRABAR</button>
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#ModalCrea"><i class="fa fa-plus-circle" aria-hidden="true"></i> MODIFICAR PASSWORD</button>
        </div>    
    </div>

</form>


<!--MODAL TAREA-->
<div class="modal fade" id="ModalCrea" role="dialog">
    <div class="modal-dialog">
    
      	<div class="modal-content">
			<div class="modal-header">
			
			<h4 class="modal-title" id="myModalLabel"> <i class="fa fa-building" aria-hidden="true"></i> Modificar Password</h4>
			</div>
			<div class="modal-body cuerpo">
				<form action="menu.php?id=35" method="post" onSubmit="return validacion();">
            	
                    <div class="wrapper-centrar wrapper-space">
                        <label for="">USUARIO</label>
                        <input type="text" name="usuario" class="form-control wrapper-centrar" value="<?= $usuario; ?>" readonly="">
                    </div>

                <div class="wrapper-centrar wrapper-space">
                    <label>INGRESAR PASSWORD</label>
                    <input type="password" name="password" class="form-control centrar" placeholder="Ingresar Password" id="pass1">
                </div>

                <div class="wrapper-centrar wrapper-space">
                    <label>CONFIRMAR PASSWORD</label>
                    <input type="password" name="passwordX" class="form-control centrar" placeholder="Confirmar Password" id="pass2">
                </div>                    

                    <div class="modal-footer wrapper-centrar">
                        <button type="submit" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i> GRABAR</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> CERRAR</button>
                    </div>			
				</form>
			</div>

      	</div>
      
    </div>
  </div>

  <script>
    function validacion(){
        var valor1 = document.getElementById('pass1').value;
        var valor2 = document.getElementById('pass2').value;
        

        if(valor1 != valor2){
            alert('La contraseña no es igual, Favor de verificar....');
            return false;
        }
        
    }
    
</script>