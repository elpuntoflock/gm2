<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
require_once 'controller/seguridad.php';
require_once 'controller/controller.php';
require_once 'db/conexion.php';

if(isset($_POST['caso']))
    {
      $caso = $_POST['caso'];  
    }
    else{
      $caso = '';        
    }

    $detalle = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, DESCRIPCION, MONTO, TIPO, MONEDA
                                    FROM tb_cargo_abono
                                    WHERE TIPO <> 6
                                    AND ID_CASO = '".$caso."'
                                    ORDER BY FECHA ASC");

$sql1 = mysqli_query($conn, "SELECT SALDO
                FROM tb_corriente
              WHERE ID_CASO = '".$caso."'");

while($rowAA = $sql1->fetch_array(MYSQLI_ASSOC)){
$saldo = $rowAA['SALDO'];
}

if (isset($saldo) <= 0){
$saldo = "0.00";
}else{
$saldo = number_format($saldo,2,'.',',');
}

$encabezado = mysqli_query($conn, "SELECT DATE_FORMAT(FECHA,'%d/%m/%Y')FEC, MONTO, MONEDA
                  									FROM tb_cargo_abono
                  									WHERE TIPO = 6
                  									  AND ID_CASO = '".$caso."'");
$CON_INICIAL = false;
while($rowA1 = $encabezado->fetch_array(MYSQLI_ASSOC)){
    $con_inicial = true;
	$monto = number_format($rowA1['MONTO'],2,'.',',');
  $fecha = $rowA1['FEC'];
  $moneda = $rowA1['MONEDA'];
}

if(isset($monto) == null){
  $moneda = " ";
}else{
  $moneda = $moneda;
}

if (isset($monto) <= 0){
	$monto = "0.00";
}else{
	$monto = $monto;
}

if (isset($fecha) == null){
	$fecha = "";
}else{
	$fecha = $fecha;
}


$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }



?>
  <script type="text/javascript">

        function selectItemByValue(elmnt, value){

        for(var i=0; i < elmnt.options.length; i++)
        {
            if(elmnt.options[i].value == value)
            elmnt.selectedIndex = i;
        }
        }

</script>

<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-return">
<a href="menu.php?id=1"><button type="button" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Regresar</button></a>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-credit-card" aria-hidden="true"></i> CUENTA CORRIENTE</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
      <div class="modal-header">
          <h4 class="modal-title">ABONOS DE CLIENTES</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <div class="modal-body">
          <form action="menu.php?id=15" method="post">
            <div class="wrapper-space wrapper-centrar">
              <label>SELECCIONAR NUMERO DE CAUSA</label>
              <select name="id_caso" class="form-control abono" id="id_caso">
                <option value="-1">SELECCIONAR CAUSA</option>
            <?php
            while ($row = mysqli_fetch_array($cta))
            {
              echo '<option value="' . $row['ID_CASO']. '">' . $row['DETALLE_CAUSA'] . ' - ' . utf8_encode($row['DESCRIPCION']) . '</option>' . "\n";
            }
            ?>                  
              </select>
              <script language="javascript">
                var numberMI = document.getElementById("id_caso");
                selectItemByValue(numberMI,<?= "'".$caso."'"?>);
            </script>              
            </div>
            <div class="wrapper-space wrapper-centrar">
              <label>DESCRIPCI&Oacute;N</label>
              <input type="text" name="desc_abono" class="form-control abono upper" placeholder="Ingresar Descripci&oacute;n">
            </div>
            <div class="wrapper-space wrapper-centrar">
              <label>MONTO</label>
              <input type="text"  name="monto" class="form-control abono upper" placeholder="Ingresar el Monto Abonar" >
            </div>
            <div class="wrapper-space wrapper-centrar">
              <label>TIPO DE ABONO</label>
              <select name="tipo" class="form-control abono">
                
                <?php
                /*CMOTTA 20190427 Se agrega para manejar si muestra o no cargo inicial */ 
                    if ($con_inicial == false) {
                        echo '<option value="6">CARGO INICIAL</option>' . "\n";    
                    }
                ?>
                
                <option value="1">CARGO NORMAL</option>
                <!--option value="2">ABONO NORMAL</option-->
                <option value="3">CHEQUE RECHAZADO</option>
                <option value="4">NOTA DE CREDITO</option>
                <option value="5">NOTA DE DEBITO</option>
                <!--option value="7">EFECTIVO</option>
                <option value="8">CHEQUE</option>
                <option value="9">TARJETA</option-->

              </select>
            </div>
            <div class="wrapper-space wrapper-centrar">
              <label>TIPO DE MONEDA</label>
              <select name="moneda" class="form-control upper">
                <option value="Q">QUETZALES</option>
                <option value="$">DOLARES</option>
              </select>
            </div>
            <div class="wrapper-space wrapper-centrar">
              <label>OBSERVACIONES</label>
              <input type="text" name="obs_abono" class="form-control abono upper" placeholder="Ingresar Observaci&oacute;n">
            </div>
            <div class="boton-formulario wrapper-space wrapper-centrar">
              <button type="submit" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i> GRABAR</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-12 wrapper-space">
      <form action="#" method="post">
        <div class="col-md-6 offset-md-2">
            <label>SELECCIONAR CAUSA</label>
            <select name="caso" class="form-control upper" id='caso' placeholder="SELECCIONAR CASO">
            <option value="-1">SELECCIONAR CAUSA</option>
                <?php
              /* CMOTTA se elimina el permiso por usuario
		 $sqlX = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION, CASE 
                                                            WHEN A.CAUSA = '' THEN 'SIN CAUSA'
                                                            ELSE A.CAUSA
                                                            END AS DETALLE_CAUSA
                                            FROM tb_caso A,
                                                tb_acceso B
                                            WHERE A.ID_CASO = B.ID_CASO");   
		*/
		 $sqlX = mysqli_query($conn, "SELECT A.ID_CASO, A.DESCRIPCION, CASE 
                                                            WHEN A.CAUSA = '' THEN 'SIN CAUSA'
                                                            ELSE A.CAUSA
                                                            END AS DETALLE_CAUSA
                                            FROM tb_caso A");

                while ($rowx = mysqli_fetch_array($sqlX))
                {
                echo '<option value="' . $rowx['ID_CASO']. '">' . utf8_encode($rowx['DETALLE_CAUSA']) .' - '. utf8_encode($rowx['DESCRIPCION']). '</option>' . "\n";
                }
                ?>           
            </select>
            <script language="javascript">
                var numberMI = document.getElementById("caso");
                selectItemByValue(numberMI,<?= "'".$caso."'"?>);
            </script>	
        </div>
        <div class="col-md-1" style="margin-top: 30px;">
            <div class="boton-formulario">
              <button type="submit" class="btn btn-success"><i class="fa fa-search" aria-hidden="true"></i> BUSCAR</button>
            </div>        
      </div>        
      </form>
  </div>

    <div class="card" style="margin-top: 100px;">
        <div class="wrapper-centrar card-header">
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal"><i class="fa fa-th-large" aria-hidden="true"></i> Realizar Cargo Abono</button>
            <a href='view/rep_cuenta_corriente.php?ftc=<?= $caso ?>' target='_blanck'><button type="button" class="btn btn-primary"><i class="fa fa-file" aria-hidden="true"></i> Reporte PDF</button></a>
        </div>
    </div>


<div class="container">

</div>

<table class="display nowrap table table-striped table-bordered table-space" style="width:100%;">
    <thead>
        <tr>
            <td>FECHA</td>
            <td>DESCRIPCI&Oacute;N</td>
            <td>MONTO</td>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td style="font-weight: bold;"><?php echo $fecha; ?></td>
        <td style="font-weight: bold;">CARGO INICIAL DE CASO:</td>
        <td style="font-weight: bold;"><?php echo $moneda; ?>&nbsp;&nbsp;<?php echo $monto; ?></td>
    </tr>     			
    </tbody>
</table>



<table id="example" class="display nowrap table table-striped table-bordered table-space" >
    <thead>
        <tr>
            <th class="centrar">FECHA</th>
            <th class="centrar">DETALLE</th>
            <th class="centrar">MONTO</th>
        </tr>
    </thead>
    <tbody>
        <?php
            while ($row = mysqli_fetch_array($detalle)){
            
                $tipo 	= $row[3];
                $plata 	= number_format($row[2],2,'.',',');
                $monedaX = $row[4];

            echo "<tr>";
                echo "<td>$row[0]</td>";
            echo "<td style='text-align: left;'>$row[1]</td>";
            if(($tipo == 2) or ($tipo == 4) or ($tipo == 5) or ($tipo == 7) or ($tipo == 8) or ($tipo == 9)){
                echo "<td style='text-align: right; color: blue; font-weight: bold;'>$monedaX $plata</td>";
            }elseif (($tipo == 1) or ($tipo == 3)) {
                echo "<td style='text-align: right; color: red; font-weight: bold;' >$monedaX $plata</td>";
            }            
                
            echo "</tr>";
            } 
        ?>       
        <tr>
            <td style="font-weight: bold;">FECHA: <?php echo date('d/m/Y') ?></td>
            <td style="font-weight: bold;">SALDO TOTAL:</td>
            <td style="font-weight: bold; text-align: right;"><?php echo $moneda.'&nbsp;&nbsp;'.$saldo; ?></td>
        </tr>  
    </tbody>
</table>  