<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
require_once 'controller/controller.php';
require_once 'controller/seguridad.php';
require_once 'db/conexion.php';

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }

?>
<style>
.wrapper-ocultar {
    display: none;
}
</style>

<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>


<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-window-restore" aria-hidden="true"></i> FACTURAS PROVEEDORES</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<form action="menu.php?id=38" method="post">
    <div class="col-md-12 wrapper-centrar wrapper-sapce">
        <div class="col-md-4">
            <label for="">SELECCIONAR PROVEEDOR</label>
            <select name="proveedor" id="proveedor" class="form-control" required="">
                <option value="">SELECCIONAR</option>
                <?php
                    while ($row = mysqli_fetch_array($proveedor))
                    {
                        echo '<option value="' . $row['ID_PROVEEDOR']. '">'. $row['NOMBRE'] . '</option>' . "\n";
                    }
                ?>
            </select>
        </div>
        <div class="col-md-3">
            <label for="">TIPO DOCUMENTO</label>
            <select name="tipo_documento" id="tipo_doc" class="form-control" >
                <option value="">SELECCIONAR</option>
                <option value="F">FACTURA</option>
                <option value="R">RECIBO</option>
            </select>
        </div>
        <div class="col-md-4 wrapper-ocultar" id="tipo_pago">
            <label for="">TIPO DE PAGO</label>
            <select name="tipo_pago" id="pago" class="form-control" autofocus="">
                <option value="">SELECCIONAR</option>
                <option value="1">CONTADO</option>
                <option value="2">CREDITO</option>
            </select>
        </div>
    </div>
    <div class="col-md-12 wrapper-space">
        <div class="col-md-4">
            <label for="">NOMBRE PROVEEDOR</label>
            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre Proveedor" readonly="">
        </div>
        <div class="col-md-2 wrapper-centrar" id="oculta_docu">
            <label for="">SERIE</label>
            <input type="text" name="serie" id="serie" class="form-control center wrapper-upper" placeholder="Serie">
        </div>
        <div class="col-md-2 wrapper-centrar">
            <label for="">FACTURA</label>
            <input type="text" name="factura" id="factura" class="form-control center" placeholder="Factura">
        </div>
        <div class="col-md-2 wrapper-centrar" id="oculta_dias">
            <label for="">DIAS</label>
            <input type="text" name="dias" id="dias" class="form-control center" placeholder="Dias">
        </div>
        <div class="col-md-2">
            <label for="">FECHA</label>
            <input type="text" name="fecha" id="datepicker" class="form-control upper center fecha" placeholder="Fecha">
        </div>
    </div>
    <div class="col-md-12 wrapper-space">
        <div class="col-md-9">
            <label for="">DESCRIPCI&Oacute;N</label>
            <input type="text" name="descripcion" class="form-control upper" placeholder="Descripci&oacute;n">
        </div>
        <div class="col-md-3">
            <label for="">MONTO</label>
            <input type="text" name="monto" class="form-control upper center" placeholder="MONTO">
        </div>
    </div>
    <div class="col-md-12 wrapper-space">
        <div class="col-md-5"></div>
        <div class="col-md-2">
            <button type="submit" id="boton" class="btn btn-success"><i class="fa fa-hdd" aria-hidden="true"></i> GRABAR</button>
        </div>
    </div>    
</form>

<script>
    $(document).ready(function(){

        $('#proveedor').change(function(){

            var proveedor = $('#proveedor').children('option:selected').val();
            
            $.post('datos_proveedor.php', {proveedor: proveedor}).done(function( respuesta )
            {
                $('#nombre').val(respuesta);
            })     
        })
    })

     $("#tipo_doc").change(function(){

        var tipo_docu = document.getElementById('tipo_doc').value;
        

        if(tipo_docu == 'R'){
            $('#serie').attr('readonly',true);
            $('#dias').attr('readonly',true);
            $('#factura').attr('readonly',true);
        }else{
            $('#serie').attr('readonly',false);
            $('#dias').attr('readonly',false);
            $('#factura').attr('readonly',false);
        }

        if(tipo_docu == 'F'){
            $('#tipo_pago').removeClass('wrapper-ocultar');
        }else{
            $('#tipo_pago').addClass('wrapper-ocultar');
        }

        

     })

     $("#pago").change(function(){

        var tipo_pago = document.getElementById('pago').value;

        if(tipo_pago == '1'){
            $('#dias').attr('readonly',true);
        }else{
            $('#dias').attr('readonly',false);
        }

     })
</script>

<script>
    $('#datepicker').datepicker({ format: 'yyyy/mm/dd' });
</script>