<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
require_once 'controller/controller.php';
require_once 'controller/seguridad.php';

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }
?>
<link rel="stylesheet" href="assets/css/style_search.css">
<script src="js/jquery.js"></script>
<script src="js/jquery.dataTables.min.js"></script>

<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="wrapper-logo-secondary">
                <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
            </div>
        </div>
    </div>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-file" aria-hidden="true"></i> DOCUMENTOS GENERAL</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12 table-responsive bajar">

<div class="col-md-2"></div>
    <div class="col-md-8">
        <header>
            <input type="search" id="input-search" class="form-control center" placeholder="Buscar archivos aqui...!!!" autofocus="">
            <div class="content-search">
                <div class="content-table">
                    <table id="table" class="col-md-8">
                        <thead>
                            <td></td>
                        </thead>
                        <tbody>
                            <?php
                                
                                $usuario 	= $_SESSION['usuario'];
                                $usuario 	= strtoupper($usuario);
                                require_once 'db/conexion.php';

                                $sql = mysqli_query($conn, "SELECT a.id_caso, a.descripcion, a.ruta
                                                                FROM tb_documento a,
                                                                    tb_acceso b,
                                                                    tb_usuario c
                                                                WHERE a.id_caso = b.ID_CASO     
                                                                AND b.ID_USUARIO = c.ID_USUARIO
                                                                AND c.ID_USUARIO = '".$usuario."'");
                
                                while ($row = mysqli_fetch_array($sql)){
                                    
                                    $info = new SplFileInfo(strtoupper($row[1]));
                                    ///echo $info;
                                    $extension = pathinfo($info->getFilename(), PATHINFO_EXTENSION);
        
                                    if($extension == 'XLS' OR $extension == 'XLSX'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/excel.jpg' />";
                                    }elseif($extension == 'PNG' OR $extension == 'JPG'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/imagenes.png' />";
                                    }elseif($extension == 'SQL'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/sql.png' />";
                                    }elseif($extension == 'PDF'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/pdf.png' />";
                                    }elseif($extension == 'DOC' OR $extension == 'DOCX'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/word.png' />";
                                    }elseif($extension == 'TXT'){
                                        $extension = "<img class='wrapper-imagen' src='img/document/txt.png' />";
                                    }                                    

                                    echo "<tr>";
                                        echo "<td><a href='$row[2]' target='_blanck'>$extension $row[1]</a></td>";
                                    echo "</tr>";
                                }

                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </header>
    </div>
</div>
<script src="js/search.js"></script>