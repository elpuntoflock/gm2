<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="lib_alert/css/sweetalert.css">
<script type="text/javascript" src="lib_alert/js/sweetalert-dev.js"></script>


<script>
	function ErrorAcceso()
		{
		swal({title:"Su usuario no tiene privilegios para esta pantalla..!", type:"error", showConfirmButton:false, text:"COMUNIQUESE CON EL ADMINISTRADOR", timer:'1500'}, 
		function () 
		{
		location.href = "menu.php?id=33"; 
		});
		}
</script>
<?php
require_once 'controller/seguridad.php';
require_once 'controller/controller.php';

$sql_permiso = mysqli_query($conn, "SELECT COUNT(*)CUENTA
										FROM tb_acceso_item
										WHERE id_usuario = '".$nombre."'
										  AND ITEM = ".$_REQUEST['id']."");
								
	while($valida = $sql_permiso->fetch_array(MYSQLI_ASSOC)){

		$resultado = $valida['CUENTA'];
	}

	if($resultado == 1){
		
	}else{
		echo "<script>ErrorAcceso();</script>";
    }


?>
<div class="row">
    <div class="col-md-12">
        <div class="wrapper-logo-secondary">
            <img src="img/logo/Law.jpg" alt="Logotipo Firma Law">
        </div>
    </div>
</div>

<div class="wrapper-title">
    <div class="container">
        <div class="row">
            <div class="col-md-4" data-line="mobil">
                <div class="line"></div>
            </div>
            <div class="col-md-4 section-title">
                <h1><i class="fa fa-address-book" aria-hidden="true"></i> PROVEEDORES</h1>
            </div>
            <div class="col-md-4">
                <div class="line"></div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="wrapper-centrar">
        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-user-plus" aria-hidden="true"></i> Crear Nuevo Proveedor</button>
    </div>
</div>

<div class="container wrapper-space" style="margin-top: 75px;">
    <div class="row">
        <div class="col-md-12">
            <table id="example" class="table table-striped table-bordered dt-responsive nowrap">
                <thead>
                    <tr>
                        <th class='center'>EDITAR</th>
                        <th class='center'>ID PROVEEDOR</th>                        
                        <th class='center'>NOMBRE PROVEEDOR</th>
                        <th class='center'>NOMBRE PROVEEDOR</th>
                        <th class='center'>CARGO CONTACTO</th>
                        <th>TEL&Eacute;FONO</th>
                        <th>CELULAR</th>
                        <th class='center'>TIPO</th>
                        <th>ESTATUS</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                   while ($row = mysqli_fetch_array($proveedor)){

                        echo "<tr>";

                            echo "<td class='wrapper-centrar'>";
                                echo "<a href='#' data-toggle='modal' data-target='#Modal' 
                                      data-id_proveedor         = '$row[7]'
                                      data-empresa              = '$row[0]'
                                      data-nombre_proveedor     = '$row[8]'
                                      data-apellido_proveedor   = '$row[9]'
                                      data-direccion            = '$row[10]'
                                      data-zona                 = '$row[11]'
                                      data-nit                  = '$row[12]'
                                      data-cui                  = '$row[13]'
                                      data-nombre_contacto      = '$row[14]'
                                      data-apellido_contacto    = '$row[15]'
                                      data-cargo                = '$row[2]'
                                      data-telefono             = '$row[3]'
                                      data-celular              = '$row[4]'
                                      data-tipo                 = '$row[16]'
                                      data-estatus              = '$row[17]'><i class='fa fa-edit' aria-hidden='true'></i> EDITAR</a>";
                            echo "</td>";                        

                            echo "<td class='wrapper-centrar'>";
                                echo $row[7];
                            echo "</td>";

                            echo "<td class='wrapper-centrar'>";
                                echo $row[0];
                            echo "</td>";

                            echo "<td class='wrapper-centrar'>";
                                echo $row[1];
                            echo "</td>";

                            echo "<td class='wrapper-centrar'>";
                                echo $row[2];
                            echo "</td>";

                            echo "<td class='wrapper-centrar'>";
                                    $telefono = $row[3];
                                if($telefono == null){
                                    $telefono = 'SIN TELEFONO';
                                }else{
                                    $telefono = $row[3];
                                }
                                echo $telefono;
                            echo "</td>";

                            echo "<td class='wrapper-centrar'>";
                                echo $row[4];
                            echo "</td>";

                            echo "<td class='wrapper-centrar'>";
                                echo $row[5];
                            echo "</td>";

                            echo "<td>";
                                echo $row[6];
                            echo "</td>";
                                                                                                                                                                                                     
                        echo "</tr>";
                    }
                    ?>                  
                </tbody>             
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-user-plus" aria-hidden="true"></i> Creaci&oacute;n Proveedores</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body cuerpo">
          <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#uploadTab" aria-controls="uploadTab" role="tab" data-toggle="tab"><i class="fa fa-plus-circle" aria-hidden="true"></i> Datos Generales</a>

                        </li>
                        <li role="presentation"><a href="#browseTab" aria-controls="browseTab" role="tab" data-toggle="tab"><i class="fa fa-plus-circle" aria-hidden="true"></i> Datos Complemento</a>

                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <form action="menu.php?id=23" method="post">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="uploadTab">
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">NOMBRE EMPRESA</label>
                                <input type="text" name="empresa" class="form-control upper" placeholder="Nombre Empresa">
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">NOMBRES PROVEEDOR</label>
                                <input type="text" name="nombre_proveedor" class="form-control upper" placeholder="Nombres Proveedor">
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">APELLIDOS PROVEEDOR</label>
                                <input type="text" name="apellido_proveedor" class="form-control upper" placeholder="Apellidos Proveedor">
                            </div> 
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">DIRECCI&Oacute;N</label>
                                <input type="text" name="direccion" class="form-control upper" placeholder="Direcci&oacute;n">
                            </div>                                

                            <div class="wrapper-space wrapper-centrar">
                                <label for="">ZONA</label>
                                <select name="zona" class="form-control upper">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="21">21</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                </select>                        
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">NIT</label>
                                <input type="text" name="nit" class="form-control upper" placeholder="Nit">
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">CUI</label>
                                <input type="text" name="cui" class="form-control upper" placeholder="Cui">
                            </div> 

                            </div>

                            <div role="tabpanel" class="tab-pane" id="browseTab">
                                
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">NOMBRES CONTACTO</label>
                                <input type="text" name="n_contacto" class="form-control upper" placeholder="Nombres Contacto">
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">APELLIDOS CONTACTO</label>
                                <input type="text" name="ap_contacto" class="form-control upper" placeholder="Apellidos Contacto">
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">CARGO CONTACTO</label>
                                <input type="text" name="cargo" class="form-control upper" placeholder="Cargo Contacto">
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">TEL&Eacute;FONO CONTACTO</label>
                                <input type="text" name="telefono" class="form-control upper" placeholder="tel&eacute;fono contacto">
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">CELULAR CONTACTO</label>
                                <input type="text" name="celular" class="form-control upper" placeholder="celular contacto">
                            </div> 
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">TIPO</label>
                                <select name="tipo" class="form-control">
                                    <option value="C">COLABORADOR</option>
                                    <option value="P">PROVEEDOR</option>
                                </select>
                            </div> 


                                <div class="wrapper-centrar wrapper-space">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i> GRABAR</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> CERRAR</button>
                                </div>                                                                                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="Modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="Modal"><i class="fa fa-user-plus" aria-hidden="true"></i>  Modificaci&oacute;n de Proveedores</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body cuerpo">
        
          <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#uploadTab1" aria-controls="uploadTab" role="tab" data-toggle="tab">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i> Datos Generales</a>

                        </li>
                        <li role="presentation"><a href="#browseTab1" aria-controls="browseTab" role="tab" data-toggle="tab">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Datos Complemento</a>

                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <form action="menu.php?id=28" method="post">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="uploadTab1">

                            <div class="wrapper-space wrapper-centrar">
                                <label for="">ID PROVEEDOR</label>
                                <input type="text" name="id_proveedor" class="form-control upper center" readonly="">
                            </div>                            
                            
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">NOMBRE EMPRESA</label>
                                <input type="text" name="empresa" class="form-control upper" placeholder="Nombre Empresa" autofocus="">
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">NOMBRES PROVEEDOR</label>
                                <input type="text" name="nombre_proveedor" class="form-control upper" placeholder="Nombres Proveedor">
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">APELLIDOS PROVEEDOR</label>
                                <input type="text" name="apellido_proveedor" class="form-control upper" placeholder="Apellidos Proveedor">
                            </div> 
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">DIRECCI&Oacute;N</label>
                                <input type="text" name="direccion" class="form-control upper" placeholder="Direcci&oacute;n">
                            </div>                                

                            <div class="wrapper-space wrapper-centrar">
                                <label for="">ZONA</label>
                                <select name="zona" class="form-control upper">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="21">21</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                </select>                        
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">NIT</label>
                                <input type="text" name="nit" class="form-control upper" placeholder="Nit">
                            </div>
                            <div class="wrapper-space wrapper-centrar">
                                <label for="">CUI</label>
                                <input type="text" name="cui" class="form-control upper" placeholder="Cui">
                            </div>                             

                            </div>

                            <div role="tabpanel" class="tab-pane" id="browseTab1">

                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">NOMBRES CONTACTO</label>
                                    <input type="text" name="n_contacto" class="form-control upper" placeholder="Nombres Contacto">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">APELLIDOS CONTACTO</label>
                                    <input type="text" name="ap_contacto" class="form-control upper" placeholder="Apellidos Contacto">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">CARGO CONTACTO</label>
                                    <input type="text" name="cargo" class="form-control upper" placeholder="Cargo Contacto">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">TEL&Eacute;FONO CONTACTO</label>
                                    <input type="text" name="telefono" class="form-control upper" placeholder="tel&eacute;fono contacto">
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">CELULAR CONTACTO</label>
                                    <input type="text" name="celular" class="form-control upper" placeholder="celular contacto">
                                </div> 
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">TIPO</label>
                                    <select name="tipo" class="form-control">
                                        <option value="C">COLABORADOR</option>
                                        <option value="P">PROVEEDOR</option>
                                    </select>
                                </div>
                                <div class="wrapper-space wrapper-centrar">
                                    <label for="">ESTATUS</label>
                                    <select name="estatus" class="form-control">
                                        <option value="A">ACTIVO</option>
                                        <option value="I">INACTIVO</option>
                                    </select>
                                </div>                                                                    

                                <div class="wrapper-centrar wrapper-space">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-plus-square" aria-hidden="true"></i> GRABAR</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-ban" aria-hidden="true"></i> CERRAR</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
      </div>


    </div>
  </div>
</div>

<script src="assets/js/bootstrap.min.js"></script>

<script>
  $('#Modal').on('show.bs.modal', function(e)
  {
      var id                 = $(e.relatedTarget).data('id_proveedor');
      var empresa            = $(e.relatedTarget).data('empresa');
      var nombre_proveedor   = $(e.relatedTarget).data('nombre_proveedor');
      var apellido_proveedor = $(e.relatedTarget).data('apellido_proveedor');
      var direccion          = $(e.relatedTarget).data('direccion');
      var zona               = $(e.relatedTarget).data('zona');
      var nit                = $(e.relatedTarget).data('nit');
      var cui                = $(e.relatedTarget).data('cui');
      var nombre_contacto    = $(e.relatedTarget).data('nombre_contacto');
      var apellido_contacto  = $(e.relatedTarget).data('apellido_contacto');
      var cargo              = $(e.relatedTarget).data('cargo');
      var telefono           = $(e.relatedTarget).data('telefono');
      var celular            = $(e.relatedTarget).data('celular');
      var tipo               = $(e.relatedTarget).data('tipo');
      var estatus            = $(e.relatedTarget).data('estatus');



      $(e.currentTarget).find('input[name="id_proveedor"]').val(id);
      $(e.currentTarget).find('input[name="empresa"]').val(empresa);
      $(e.currentTarget).find('input[name="nombre_proveedor"]').val(nombre_proveedor);
      $(e.currentTarget).find('input[name="apellido_proveedor"]').val(apellido_proveedor);
      $(e.currentTarget).find('input[name="direccion"]').val(direccion);
      $(e.currentTarget).find('select[name="zona"]').val(zona);
      $(e.currentTarget).find('input[name="nit"]').val(nit);
      $(e.currentTarget).find('input[name="cui"]').val(cui);
      $(e.currentTarget).find('input[name="n_contacto"]').val(nombre_contacto);
      $(e.currentTarget).find('input[name="ap_contacto"]').val(apellido_contacto);
      $(e.currentTarget).find('input[name="cargo"]').val(cargo);
      $(e.currentTarget).find('input[name="telefono"]').val(telefono);
      $(e.currentTarget).find('input[name="celular"]').val(celular);
      $(e.currentTarget).find('select[name="tipo"]').val(tipo);
      $(e.currentTarget).find('select[name="estatus"]').val(estatus);


  });
</script>