<?php
session_start();
require_once '../fpdf/fpdf.php';
require_once '../db/conexion.php';
$nombre = strtoupper($_SESSION['usuario']);


$juzgado      = utf8_decode('Descripción Juzgado');
$descri       = utf8_decode('Descripción del Caso');

$detalle = mysqli_query($conn, "SELECT A.CAUSA, CONCAT(B.NOMBRES,' ',B.APELLIDOS)NOMBRES, A.JUZGADO, A.DESCRIPCION,
                                        DATE_FORMAT(A.FECHA_INI,'%d/%m/%Y'), DATE_FORMAT(A.FECHA_FIN,'%d/%m/%Y'), A.ABOGA_RESPONSABLE
                                FROM tb_caso A,
                                    tb_contacto B,
                                    tb_acceso C
                                WHERE A.ID_CONTACTO = B.ID_CONTACTO
                                AND A.ID_CASO 	= C.ID_CASO
                                AND C.ID_USUARIO  = '".$nombre."'");

$pdf = new FPDF('L');
$pdf->AddPage();
$pdf->SetFont('Arial','',7);

$pdf->Image('../img/logo/Law.jpg',25,15,35,0);

$pdf->SetY(50);

$pdf->Cell(275,10,'REPORTE GENERAL DE CASOS POR USUARIO',0,1,'C');

$pdf->Cell(30,5,'Causa', 1,0,'C');
$pdf->Cell(50,5,'Nombre del Cliente', 1,0,'C');
$pdf->Cell(50,5,$juzgado, 1,0,'C');
$pdf->Cell(70,5,$descri, 1,0,'C');
$pdf->Cell(40,5,'Abogado Responsable', 1,0,'C');
$pdf->Cell(25,5,'Fecha Inicio', 1,1,'C');


  
while ($rest = mysqli_fetch_array($detalle)){

    $causa          = $rest[0];
    $nombre         = ucwords(strtolower($rest[1]));
    $juzgado        = ucwords(strtolower($rest[2]));
    $descripcion    = $rest[3];
    $fecha_ini      = $rest[4];
    $fecha_fin      = strtolower($rest[5]);
    $aboga          = ucwords(strtolower($rest[6]));
    
    $pdf->Cell(30,5, $causa , 1, 0, 'L');
    $pdf->Cell(50,5, $nombre ,1, 0, 'L');
    $pdf->Cell(50,5, $juzgado ,1, 0, 'L');
    $pdf->Cell(70,5, $descripcion ,1, 0, 'L');
    $pdf->Cell(40,5, $aboga ,1, 0, 'L');
    $pdf->Cell(25,5, $fecha_ini ,1, 1, 'C');
    

}

$pdf->Output();

?>
